const CartController = require("./../Controllers/CartController");
const ItemsController = require("./../Controllers/ItemsController");


module.exports = function (express) {


    let subApp = express();


    subApp.get("/cart", CartController.index);
    subApp.get("/items", ItemsController.index);


    return subApp;


}