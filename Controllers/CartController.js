const Cart = require("./../Models/Cart");
const CartResource = require("./../ApiResources/CartResource");


class CartController {

    static index(req, res) {

        let cart = new Cart();

        cart.getWithItems().then(function (r) {
            res.json(CartResource.collection(r));
        }).catch(err => {
            res.json({status: false, message: "some thing wrong happen", err});
        })

    }

}


module.exports = CartController;