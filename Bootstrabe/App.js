const express = require("express");
const bodyParser = require('body-parser');

const routes = {
    api: require("./../Routes/Api")(express)
};

// const frontEnd = require("./Routes/Front")(express);

module.exports = class Bootstrap {
    constructor() {

    }

    serveStatic(staticDir) {
        this.staticDir = staticDir;
        return this;
    }

    start(port) {
        const app = express();

        let extended = false;
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({extended}));

        if (this.staticDir)
            app.use(express.static(this.staticDir));

        // console.log(routes);
        Object.keys(routes).forEach(key => app.use(`/${key}`, routes[key]));

        // app.use("/", frontEnd);
        app.listen(port, () => console.log(`your app run on port ${port}`))
    }


}

// module.exports = {Bootstrap};