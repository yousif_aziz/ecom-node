const mysql = require("mysql2");


class BaseModel {

    constructor() {
        this.con = mysql.createConnection({
            host: process.env.DB_HOST,
            user: process.env.DB_USERNAME,
            database: process.env.DB_DATABASE,
            password: process.env.DB_PASSWORD
        });
    }


    query(sql) {
        return new Promise((resolve, reject) => {
            this.con.connect((err) => {
                if (err)
                    return reject(err);

                this.con.query(sql, function (err, result) {
                    if (err)
                        return reject(err);
                    return resolve(result);
                });

            });
        })
    }


    get() {
        return this.query(`select * from ${this.tableName}`)
    }


    paginate(perPage = 10) {

    }


}

module.exports = BaseModel;