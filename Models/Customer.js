import {sequelize, Sequelize, Model} from "./BaseModel";


class Customer extends Model {
}

Customer.init({first_name: {type: Sequelize.STRING, allowNull: false}},
    {sequelize, modelName: 'customer', timestamps: false}
);


module.exports = Customer;