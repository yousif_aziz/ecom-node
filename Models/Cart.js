const BaseModel = require("./BaseModel");

class Cart extends BaseModel {
    constructor() {
        super();
        this.tableName = "cart";

    }


    getWithItems() {
        return this.query(`select * from ${this.tableName} left join items on cart.item_id = items.id`)
    }


}


module.exports = Cart;