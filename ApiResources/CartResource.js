class CartResource {

    static collection(collection) {
        return collection.map(item => {
            return (new CartResource(item)).toArray();
        })
    }


    constructor(item) {
        Object.keys(item).forEach(key => {
            this[key] = item[key];
        })
    }


    toArray() {
        return {
            "id": this.id,
            "quantity": this.quantity,
            "item": {
                "name": this.name,
                "cartCount": this.quantity,
                "slug": this.id + "-" + this.name,
                "price": this.price,
                "mini_description": this.description
            }

        }
    }
}


module.exports = CartResource;